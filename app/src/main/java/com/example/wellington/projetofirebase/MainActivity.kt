package com.example.wellington.projetofirebase

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    // Instanciando Objetos
    lateinit var nome: EditText
    lateinit var qtdEpisodios: EditText
    lateinit var origem: EditText
    lateinit var avaliacao: RatingBar
    lateinit var salvar: Button
    lateinit var listar: Button

    lateinit var ref: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        // Referencia com o Firebase
        ref = FirebaseDatabase.getInstance().getReference("Series")

        // Setando Valores
        nome            = findViewById(R.id.serieNome)
        qtdEpisodios    = findViewById(R.id.serieEpisodios)
        origem          = findViewById(R.id.serieOrigem)
        avaliacao       = findViewById(R.id.ratingBar)
        salvar          = findViewById(R.id.btnSave)
        listar          = findViewById(R.id.btnListar)

        salvar.setOnClickListener{
            salvarSerie()
        }

        listar.setOnClickListener{
            val intent = Intent(this@MainActivity, ListarSeries::class.java)
            startActivity(intent)
        }

    }

    private fun salvarSerie(){
        val name        = nome.text.toString().trim()
        val episodes    = qtdEpisodios.text.toString().trim()
        val origin      = origem.text.toString().trim()

        if(name.isEmpty()){
            nome.error = "Preencha um nome da série"
            return
        }
        if(episodes.isEmpty()){
            qtdEpisodios.error = "Preencha a quantidade de episódios"
            return
        }
        if(origin.isEmpty()){
            origem.error = "Preencha onde assistir a série"
            return
        }

        // Id único da Série
        val serieID = ref.push().key
        // Criando Serie
        val serie = Serie(serieID, name, episodes, origin, avaliacao.rating.toInt())

        ref.child(serieID).setValue(serie).addOnCompleteListener{
            Toast.makeText(applicationContext, "Série salva!", Toast.LENGTH_LONG).show()
            nome.setText("")
            qtdEpisodios.setText("")
            origem.setText("")
        }

    }
}
