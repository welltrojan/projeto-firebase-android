package com.example.wellington.projetofirebase

import android.os.Bundle
import android.renderscript.Sampler
import android.support.v7.app.AppCompatActivity
import android.widget.AdapterView
import android.widget.ListView
import com.google.firebase.database.*
import android.widget.ArrayAdapter



class ListarSeries : AppCompatActivity() {

    lateinit var ref: DatabaseReference
    lateinit var serieList: MutableList<Serie>
    lateinit var listView: ListView
    lateinit var arrayadapter: ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.listar_series)

        // Referencia com o Firebase
        ref = FirebaseDatabase.getInstance().getReference("Series")
        serieList = mutableListOf()
        listView = findViewById(R.id.listaSeries)

        ref.addValueEventListener(object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError?) {
            }

            override fun onDataChange(p0: DataSnapshot?) {
                if(p0!!.exists()){
                    for(s in p0.children){
                        val serie = s.getValue(Serie::class.java)
                        serieList.add(serie!!)
                    }

                    val adapter = SerieAdapter(applicationContext, R.layout.serie_item, serieList)
                    listView.adapter = adapter

                }
            }

        })

//        listView.setOnItemLongClickListener(){
//
//        }

    }

}