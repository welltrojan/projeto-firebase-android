package com.example.wellington.projetofirebase

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class SerieAdapter(val mCtx: Context, val layoutResId: Int, val serieList: List<Serie>) : ArrayAdapter<Serie>(mCtx,layoutResId, serieList) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layoutInflater: LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(layoutResId, null )

        val textViewName = view.findViewById<TextView>(R.id.serie_item_nome)
        val textViewEpisodes = view.findViewById<TextView>(R.id.serie_item_episodios)
        val textViewOrigin = view.findViewById<TextView>(R.id.serie_item_origem)

        val serie = serieList[position]
        textViewName.text = "Nome: " + serie.nome
        textViewEpisodes.text = "Episódios: " + serie.episodios
        textViewOrigin.text = "Onde Assistir: " + serie.origem

        return view
    }
}